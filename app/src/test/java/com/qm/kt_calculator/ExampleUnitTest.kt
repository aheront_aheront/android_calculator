package com.qm.kt_calculator

import android.widget.TextView
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

class ExampleUnitTest {
    var view = object : ICalcView{
        private val textView = TextView(CalcActivity())
        override fun getInputLine(): TextView {
            return textView
        }

        override fun getMaxInputLenght(): Int {
            return 10
        }

    }
    var presenter = CalcPresenter(
        view,
        CalcModel()
    )

    @Test
    fun addition_isCorrect() {
        presenter.digitPressed(5)
        assert(view.getInputLine().text.toString().equals("5"))
    }
}