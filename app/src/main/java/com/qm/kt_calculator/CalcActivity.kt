package com.qm.kt_calculator

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView


class CalcActivity : AppCompatActivity(), ICalcView {

    private lateinit var inputLine: TextView
    private lateinit var presenter: CalcPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calc)
        init()
    }

    private fun init() {
        val model = CalcModel()
        presenter = CalcPresenter(model)
        presenter.attachView(this)
        inputLine = findViewById(R.id.calcInputLine)

        findViewById<Button>(R.id.calcBtnDigit0).setOnClickListener { presenter.digitPressed(0) }
        findViewById<Button>(R.id.calcBtnDigit1).setOnClickListener { presenter.digitPressed(1) }
        findViewById<Button>(R.id.calcBtnDigit2).setOnClickListener { presenter.digitPressed(2) }
        findViewById<Button>(R.id.calcBtnDigit3).setOnClickListener { presenter.digitPressed(3) }
        findViewById<Button>(R.id.calcBtnDigit4).setOnClickListener { presenter.digitPressed(4) }
        findViewById<Button>(R.id.calcBtnDigit5).setOnClickListener { presenter.digitPressed(5) }
        findViewById<Button>(R.id.calcBtnDigit6).setOnClickListener { presenter.digitPressed(6) }
        findViewById<Button>(R.id.calcBtnDigit7).setOnClickListener { presenter.digitPressed(7) }
        findViewById<Button>(R.id.calcBtnDigit8).setOnClickListener { presenter.digitPressed(8) }
        findViewById<Button>(R.id.calcBtnDigit9).setOnClickListener { presenter.digitPressed(9) }
        findViewById<Button>(R.id.calcBtnDot).setOnClickListener { presenter.dotPressed() }

        findViewById<Button>(R.id.calcBtnAC).setOnClickListener { presenter.acPressed() }

        findViewById<Button>(R.id.calcBtnPlus).setOnClickListener { presenter.operatorPressed(Operator.Plus) }
        findViewById<Button>(R.id.calcBtnMinus).setOnClickListener { presenter.operatorPressed(Operator.Minus) }
        findViewById<Button>(R.id.calcBtnMultiply).setOnClickListener{presenter.operatorPressed(Operator.Multiply)}
        findViewById<Button>(R.id.calcBtnDivide).setOnClickListener{presenter.operatorPressed(Operator.Divide)}

        findViewById<Button>(R.id.calcBtnEqual).setOnClickListener { presenter.evalPressed() }
        presenter.acPressed()
    }

    override fun getInputLine(): TextView {
        return inputLine
    }

    override fun getMaxInputLenght(): Int {

        return resources.getString(R.string.calcInputLinePlaceholders).length

    }

    override fun getErrorMessage(): String {
        return resources.getString(R.string.calcInputLineError)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

}