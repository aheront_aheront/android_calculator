package com.qm.kt_calculator

import android.widget.TextView

interface ICalcView {
    fun getInputLine() : TextView
    fun getMaxInputLenght() : Int
    fun getErrorMessage() : String
}