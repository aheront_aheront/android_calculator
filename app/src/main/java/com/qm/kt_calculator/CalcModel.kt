package com.qm.kt_calculator

import android.util.Log
import java.math.BigDecimal
import java.math.RoundingMode

enum class Operator{
    Plus, Minus, Multiply, Divide
}

class CalcModel {
    private var numberList : MutableList<BigDecimal> = MutableList(0) {BigDecimal(0)}
    private var operatorList : MutableList<Operator> = MutableList(0){Operator.Plus}
    private val DEFAULT_ANSWER = BigDecimal.ZERO

    fun appendNumber(number: BigDecimal){
        numberList.add(number)
        Log.d("model123", numberList.toString())
    }

    fun appendOperator(operator: Operator) {
        operatorList.add(operator)
        Log.d("model123", operatorList.toString())
    }

    fun reset(){
        numberList.clear()
        Log.d("model123", numberList.toString())

        operatorList.clear()
        Log.d("model123", operatorList.toString())

    }

    fun evalResult() : BigDecimal{
        if(numberList.size == 0) return DEFAULT_ANSWER
        if(numberList.size == operatorList.size){
            val result = evalPreResult()
            return evalDefaultOperator(result, operatorList[operatorList.lastIndex])
        }

        var result = numberList[0]
        for(i in operatorList.indices){
            result = evalOperator(result, numberList[i+1], operatorList[i])
        }
        return result
    }

    fun evalPreResult() : BigDecimal{
        if(numberList.size == 0){
            return DEFAULT_ANSWER
        }
        var result = numberList[0]
        for(i in 0 until operatorList.size-1){
            result = evalOperator(result, numberList[i+1], operatorList[i])
        }
        return result
    }

    fun redoLastOperator() : BigDecimal{
        if(operatorList.size == 0)
            return evalResult()
        appendOperator(operatorList[operatorList.lastIndex])
        appendNumber(numberList[numberList.lastIndex])
        return evalResult()
    }

    fun changeLastOperator(operator: Operator){
        operatorList[operatorList.size-1] = operator
    }

    private fun evalDefaultOperator(a: BigDecimal, operator: Operator) : BigDecimal{
        return when (operator) {
            Operator.Plus -> a
            Operator.Minus -> BigDecimal.ZERO - a
            Operator.Multiply -> a * a
            Operator.Divide -> BigDecimal.ONE.divide(a, 10,RoundingMode.UP)
        }
    }
    private fun evalOperator(a:BigDecimal, b:BigDecimal, operator: Operator):BigDecimal{
        return when (operator) {
            Operator.Plus -> a + b
            Operator.Minus -> a - b
            Operator.Multiply -> a * b
            Operator.Divide -> a.divide(b, 10,RoundingMode.UP)
        }
    }


}