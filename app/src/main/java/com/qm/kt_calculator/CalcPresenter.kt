package com.qm.kt_calculator

import android.annotation.SuppressLint
import java.math.BigDecimal
import com.qm.kt_calculator.ICalcView as ICalcView1

enum class InputLineState{Number, Answer, PreResult, Null, Error}

class CalcPresenter(private val model: CalcModel) {
    private var view: ICalcView1? = null
    private var inputLineState = InputLineState.Null

    fun attachView(view: ICalcView1){
        this.view = view
    }

    fun detachView(){
        view = null
    }

    @SuppressLint("SetTextI18n")
    fun digitPressed(digit: Int) {
        when(inputLineState){
            InputLineState.Error -> return

            InputLineState.Null, InputLineState.PreResult ->{
                view!!.getInputLine().text = digit.toString()
            }

            InputLineState.Number ->
                if (canAppendNewDigit())
                    view!!.getInputLine().text = view!!.getInputLine().text.toString() + digit.toString()

            InputLineState.Answer ->{
                model.reset()
                view!!.getInputLine().text = digit.toString()
            }
        }
        inputLineState = InputLineState.Number
    }

    @SuppressLint("SetTextI18n")
    fun dotPressed(){
        when(inputLineState){
            InputLineState.Error -> return

            InputLineState.Answer ->
                acPressed()

            InputLineState.PreResult->
                view?.getInputLine()?.text = "0"
        }
        if(!view!!.getInputLine().text.contains('.'))
            view!!.getInputLine().text = view!!.getInputLine().text.toString() + "."
        inputLineState = InputLineState.Number
    }

    private fun canAppendNewDigit(): Boolean {
        var inputtedLength = view!!.getInputLine().text.length
        inputtedLength += if(view!!.getInputLine().text.contains('.')) -1 else 0
        return  inputtedLength + 1 <= view!!.getMaxInputLenght()
    }

    fun acPressed() {
        when(inputLineState){
            InputLineState.Answer, InputLineState.PreResult, InputLineState.Error ->
                model.reset()
        }
        view!!.getInputLine().text = "0"
        inputLineState = InputLineState.Null
    }

    fun operatorPressed(operator: Operator) {
        if(inputLineState == InputLineState.Error)
            return

        if(inputLineState == InputLineState.PreResult){
            model.changeLastOperator(operator)
            return
        }

        if(inputLineState == InputLineState.Answer)
            model.reset()

        model.appendNumber(view!!.getInputLine().text.toString().toBigDecimal())
        model.appendOperator(operator)
        try {
            view!!.getInputLine().text = normalizeToView(model.evalPreResult())
        } catch (e : Exception){
            view!!.getInputLine().text = view!!.getErrorMessage()
            inputLineState = InputLineState.Error
            return
        }
        inputLineState = InputLineState.PreResult
    }


    fun evalPressed() {
        if(inputLineState == InputLineState.Error)
            return

        if(inputLineState == InputLineState.Answer){
            try {
                view!!.getInputLine().text = normalizeToView(model.redoLastOperator())
            } catch (e : Exception){
                view!!.getInputLine().text = view!!.getErrorMessage()
                inputLineState = InputLineState.Error
                return
            }
            return
        }

        model.appendNumber(view!!.getInputLine().text.toString().toBigDecimal())
        try {
            view!!.getInputLine().text = normalizeToView(model.evalResult())
        } catch (e : Exception){
            view!!.getInputLine().text = view!!.getErrorMessage()
            inputLineState = InputLineState.Error
            return
        }
        inputLineState = InputLineState.Answer
    }


    private fun normalizeToView(number: BigDecimal): String{
        if((number % BigDecimal.ONE).toString() == "0.0" ) return number.toInt().toString()
        var result = ""
        var isNull = number.toString().contains('.')
        for(c in number.toString().reversed()){
            when{
                c != '0' && c != '.' -> isNull = false
            }
            if(isNull)
               continue
            result = c + result
        }
        return result
    }

}